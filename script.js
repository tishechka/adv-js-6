/*
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

Асинхронність у JavaScript відноситься до способу обробки операцій, які не чекають завершення однієї операції перед початком іншої.
Тобто, коли виконується асинхронний код, програма не зупиняється чекати завершення конкретної дії,
а замість цього продовжує виконання інших операцій.
*/

document.getElementById('findIpBtn').addEventListener('click', async () => {
    try {
        const ipifyResponse = await fetch('https://api.ipify.org/?format=json');
        const { ip } = await ipifyResponse.json();

        const ipApiResponse = await fetch(`https://ipapi.co/${ip}/json/`);
        const ipInfo = await ipApiResponse.json();

        const ipInfoElement = document.getElementById('ipInfo');
        ipInfoElement.innerHTML = `
                    <p>Континент: ${ipInfo.region}</p>
                    <p>Країна: ${ipInfo.country_name}</p>
                    <p>Регіон: ${ipInfo.region}</p>
                    <p>Місто: ${ipInfo.city}</p>
                    <p>Район: ${ipInfo.region}</p>
                `;
    } catch (error) {
        console.error('Помилка при отриманні інформації:', error);
    }
});